FROM golang:1.18-bullseye

ENV APP_HOME /go/src/kependudukan
WORKDIR "$APP_HOME"
COPY . .
RUN go mod download
RUN go mod verify